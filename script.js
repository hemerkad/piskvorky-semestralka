document.addEventListener('DOMContentLoaded', () => {
    const boardElement = document.getElementById('board');
    const newGameBtn = document.getElementById('newGameBtn');
    const replayGameBtn = document.getElementById('replayGameBtn');
    const toggleModeBtn = document.getElementById('toggleModeBtn');
    const gameDurationElement = document.getElementById('gameDuration');
    const moveCountElement = document.getElementById('moveCount');
    const boardSizeElement = document.getElementById('boardSize');
    const volumeControl = document.getElementById('volumeControl');
    const backgroundMusic = document.getElementById('backgroundMusic');
    const difficultySelect = document.getElementById('difficulty');

    let boardSize;
    let board = [];
    let currentPlayer = 'X';
    let gameMoves = [];
    let isTwoPlayerMode = false;
    let gameInterval;
    let gameStartTime;
    let gameOver = false;
    let gameEndTime;
    let playerXScore = 0;
    let playerOScore = 0;
    
    /**
     * Aktualizuje skore
     */
    function updateScores() {
        document.getElementById('playerXScore').textContent = playerXScore;
        document.getElementById('playerOScore').textContent = playerOScore;
    }

    /**
     * nastavení velikosti hrací desky
     * vytvoření hrací desky
     * vytvoření buněk hrací desky
     * aktualizace zobrazení
     */

    function initializeBoard() {
        const difficulty = difficultySelect.value;
        if (difficulty === 'easy') {
            boardSize = 3;
        } else if (difficulty === 'medium') {
            boardSize = 5;
        } else {
            boardSize = 7;
        }
    
        board = Array(boardSize).fill().map(() => Array(boardSize).fill(null));
        boardElement.innerHTML = '';
        boardElement.style.gridTemplateColumns = `repeat(${boardSize}, 100px)`;

        const cellSize = 100;
        const gapSize = 5;
        const boardPadding = 5 * 2;
        const boardWidth = cellSize * boardSize + gapSize * (boardSize - 1) + boardPadding;
        const boardHeight = cellSize * boardSize + gapSize * (boardSize - 1) + boardPadding;

        boardElement.style.width = `${boardWidth}px`;
        boardElement.style.height = `${boardHeight}px`;
    
        for (let i = 0; i < boardSize * boardSize; i++) {
            const cell = document.createElement('div');
            cell.classList.add('cell');
            cell.dataset.index = i;
            cell.addEventListener('click', onCellClick);
            boardElement.appendChild(cell);
        }

        boardSizeElement.textContent = `${boardSize}x${boardSize}`;
        gameMoves = [];
        currentPlayer = 'X';
        gameStartTime = new Date();
        clearInterval(gameInterval);
        gameInterval = setInterval(updateGameDuration, 1000);
        gameDurationElement.textContent = '0';
        moveCountElement.textContent = '0';
        replayGameBtn.disabled = true;
        gameOver = false;
        updateScores();
    }
    
    /**
     * získání aktuálního času
     * výpočet trvání hry
     * aktualizace zobrazení
     */

    function updateGameDuration() {
        const currentTime = new Date();
        const duration = Math.floor((currentTime - gameStartTime) / 1000);
        gameDurationElement.textContent = duration;
    }

    /**
     * kontrola stavu hry
     * získání indexu buňky
     * přepočet řádku a sloupce
     * kontrola volné buňky
     * zaznamenání tahu
     * kontrola výhry
     * kontrola remízy
     * přepnutí hráče
     * tah AI
     */
    function onCellClick(event) {
        if (gameOver) return;

        const index = event.target.dataset.index;
        const row = Math.floor(index / boardSize);
        const col = index % boardSize;
        if (!board[row][col]) {
            board[row][col] = currentPlayer;
            event.target.textContent = currentPlayer;
            gameMoves.push({ player: currentPlayer, row, col });
            moveCountElement.textContent = gameMoves.length;

            if (checkWin(row, col)) {
                if (currentPlayer === 'X') {
                    playerXScore++;
                } else {
                    playerOScore++;
                }
                endGame(`${currentPlayer} vyhrál!`);
                return;
            }

            if (gameMoves.length === boardSize * boardSize) {
                endGame('Remíza!');
                return;
            }

            currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
            if (!isTwoPlayerMode && currentPlayer === 'O') {
                setTimeout(makeAIMove, 100);
            }
        }
    }

    /**
     * Nastavení požádované délky výherní řady
     * Kontrola výhernách podmínek
     * Výsledek (Funkce vrátí true, pokud je nalezena výherní řada v kterémkoli ze čtyř směrů.
     * V opačném případě vrátí false.)
     */
    function checkWin(row, col) {
        const winCondition = boardSize === 3 ? 3 : 4;
        return checkLine(row, col, 1, 0, winCondition) ||
            checkLine(row, col, 0, 1, winCondition) ||
            checkLine(row, col, 1, 1, winCondition) ||
            checkLine(row, col, 1, -1, winCondition);
    }

    /**
     * parametry funkce (row a col jsou souřadnice buňky, od které začínáme kontrolovat řadu.
     * rowDir a colDir určují směr, kterým budeme procházet buňky (např. 1, 0 pro vertikální kontrolu).
     * winCondition je požadovaná délka vítězné řady.)
     * Inicializace proměnných
     * Kontrola směru
     * Výsledek (Funkce vrátí true, pokud jsme našli výherní řadu, tj. pokud count dosáhne nebo překročí winCondition.
     * V opačném případě vrátí false.)
    */
    function checkLine(row, col, rowDir, colDir, winCondition) {
        const symbol = board[row][col];
        let count = 0;

        for (let i = 0; i < winCondition; i++) {
            const newRow = row + i * rowDir;
            const newCol = col + i * colDir;
            if (newRow >= 0 && newRow < boardSize && newCol >= 0 && newCol < boardSize && board[newRow][newCol] === symbol) {
                count++;
            } else {
                break;
            }
        }

        for (let i = 1; i < winCondition; i++) {
            const newRow = row - i * rowDir;
            const newCol = col - i * colDir;
            if (newRow >= 0 && newRow < boardSize && newCol >= 0 && newCol < boardSize && board[newRow][newCol] === symbol) {
                count++;
            } else {
                break;
            }
        }

        return count >= winCondition;
    }

    /**
     * Zastavení časovače hry
     * zobrazení zprávy
     * povolení tlačítka pro přehrání hry
     * nastavení stavu hry = gameOver na true
     * aktualizace skore
     * 
     * vypočet času ukončení hry(Vypočítá se doba trvání hry (gameEndTime) odečtením času začátku hry (gameStartTime)
     * od aktuálního času (new Date()). Tento čas je převeden na sekundy pomocí funkce Math.floor().)
     */

    function endGame(message) {
        clearInterval(gameInterval);
        alert(message);
        replayGameBtn.disabled = false;
        gameOver = true;
        updateScores();
        gameEndTime = Math.floor((new Date() - gameStartTime) / 1000);
    }

    /**
     * Získání obtížnosti
     * Získání tahu AI podle obtížnosti
     * Provedení tahu
     * Tímto způsobem je AI schopna reagovat na různé obtížnosti a pokusit se hrát strategicky podle nastavení.
     */

    function makeAIMove() {
        const difficulty = difficultySelect.value;
        let move;
        if (difficulty === 'easy') {
            move = getRandomMove();
        } else if (difficulty === 'medium') {
            move = getWinningMove() || getBlockingMove() || getRandomMove();
        } else {
            move = getBestMove();
        }
        if (move) {
            const cellIndex = move.row * boardSize + move.col;
            document.querySelector(`.cell[data-index='${cellIndex}']`).click();
        }
    }

    /**
     * Inicializace prázdného pole
     * procházení hrací desky
     * náhodný výběr tahu
     * Tímto způsobem funkce getRandomMove() umožňuje AI provádět náhodné tahy na hracím plánu.
     */
    function getRandomMove() {
        const emptyCells = [];
        for (let row = 0; row < boardSize; row++) {
            for (let col = 0; col < boardSize; col++) {
                if (!board[row][col]) {
                    emptyCells.push({ row, col });
                }
            }
        }
        return emptyCells[Math.floor(Math.random() * emptyCells.length)];
    }

    /**
     * procházení hrací desky
     * testování blokování výhry (Pokud je buňka prázdná (tj. hodnota v ní je null), dočasně umístí symbol hráče X ('X') do této buňky.)
     * vracení výsledku
     * Tímto způsobem funkce getBlockingMove() umožňuje AI identifikovat a provádět tahy, které zabraňují výhře hráče X.
     */

    function getBlockingMove() {
        for (let row = 0; row < boardSize; row++) {
            for (let col = 0; col < boardSize; col++) {
                if (!board[row][col]) {
                    board[row][col] = 'X';
                    const playerWin = checkWin(row, col);
                    board[row][col] = null;
                    if (playerWin) {
                        return { row, col };
                    }
                }
            }
        }
        return null;
    }

    /** 
     * procházení hrací desky
     * testování vátězného tahu
     * vrácení výsledku
     * Tímto způsobem funkce getWinningMove() umožňuje AI identifikovat a provádět tahy, které vedou k vlastnímu vítězství.
     */

    function getWinningMove() {
        for (let row = 0; row < boardSize; row++) {
            for (let col = 0; col < boardSize; col++) {
                if (!board[row][col]) {
                    board[row][col] = 'O';
                    const aiWin = checkWin(row, col);
                    board[row][col] = null;
                    if (aiWin) {
                        return { row, col };
                    }
                }
            }
        }
        return null;
    }

    /*  Inicialiuace proměnných, procházení všech polí hrací desky, kontrola prázdných cellů, výběr nejlepšího tahu kterýho vrací
    *   Prochází všechny tahy AI a vybírá ten, který je považován za nejlepší vzhledem k aktuální situaci na hrací desce.
    */

    function getBestMove() {
        const maxDepth = 3;
        let bestMove;
        let bestValue = -Infinity;
        for (let row = 0; row < boardSize; row++) {
            for (let col = 0; col < boardSize; col++) {
                if (!board[row][col]) {
                    board[row][col] = 'O';
                    const moveValue = minimax(board, 0, false, -Infinity, Infinity, maxDepth);
                    board[row][col] = null;
                    if (moveValue > bestValue) {
                        bestValue = moveValue;
                        bestMove = { row, col };
                    }
                }
            }
        }
        return bestMove;
    }

    /** 
     *  vyhodnocení koncových stavů, 
     *  rekurzivní prohledávání stavového prostoru - (Pokud hra neskončila a nebyla dosažena maximální hloubka prohledávání (maxDepth), 
     *   algoritmus pokračuje v rekurzivním prohledávání možných tahů.)
     *   výběr nejlepšího tahu
     *   Tímto způsobem funkce minimax() prochází stavový prostor hry a vyhodnocuje nejlepší tahy pro oba hráče s ohledem na jejich strategii.
    */

    function minimax(board, depth, isMaximizing, alpha, beta, maxDepth) {
        if (checkWinCondition('O')) {
            return 10 - depth;
        }
        if (checkWinCondition('X')) {
            return depth - 10;
        }
        if (isBoardFull() || depth === maxDepth) {
            return 0;
        }
        if (isMaximizing) {
            let bestValue = -Infinity;
            for (let row = 0; row < boardSize; row++) {
                for (let col = 0; col < boardSize; col++) {
                    if (!board[row][col]) {
                        board[row][col] = 'O';
                        bestValue = Math.max(bestValue, minimax(board, depth + 1, false, alpha, beta, maxDepth));
                        board[row][col] = null;
                        alpha = Math.max(alpha, bestValue);
                        if (beta <= alpha) {
                            return bestValue;
                        }
                    }
                }
            }
            return bestValue;
        } else {
            let bestValue = Infinity;
            for (let row = 0; row < boardSize; row++) {
                for (let col = 0; col < boardSize; col++) {
                    if (!board[row][col]) {
                        board[row][col] = 'X';
                        bestValue = Math.min(bestValue, minimax(board, depth + 1, true, alpha, beta, maxDepth));
                        board[row][col] = null;
                        beta = Math.min(beta, bestValue);
                        if (beta <= alpha) {
                            return bestValue;
                        }
                    }
                }
            }
            return bestValue;
        }
    }

    /**
     * Proházení všech cellů na herní desce
     * Kontrola výhry na základě aktuálního cellu
     * vracení výsledku
     * Tímto způsobem funkce checkWinCondition(player) kontroluje všechna pole hracího plánu a vrací true, pokud hráč player vyhrál, a false v opačném případě.
     */

    function checkWinCondition(player) {
        for (let row = 0; row < boardSize; row++) {
            for (let col = 0; col < boardSize; col++) {
                if (board[row][col] === player && checkWin(row, col)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Prochází všechny celly herní desky
     * Kontrola obsazenosti cellů
     * Vracení výsledku - pokud je deska plná vrací se true
     * Tímto způsobem funkce isBoardFull() zjišťuje, zda je hrací plán plný nebo obsahuje ještě volná pole.
     */

    function isBoardFull() {
        for (let row = 0; row < boardSize; row++) {
            for (let col = 0; col < boardSize; col++) {
                if (!board[row][col]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Inicializace proměnné moveIndex
     * Inicializace hrací desky beu resetování tahů
     * Spuštění intervalu pro přehrávání
     * Konec přehrávání = Jakmile je moveIndex rovno délce pole gameMoves, tedy všechny tahy byly přehrány, interval se zastaví pomocí clearInterval
     * Tímto způsobem funkce replayGame() umožňuje přehrát předchozí hru krok za krokem s určeným časovým intervalem mezi tahy.
     */

    function replayGame() {
        let moveIndex = 0;
        initializeBoardWithoutResettingMoves();
        const replayInterval = setInterval(() => {
            if (moveIndex < gameMoves.length) {
                const move = gameMoves[moveIndex];
                const cellIndex = move.row * boardSize + move.col;
                document.querySelector(`.cell[data-index='${cellIndex}']`).textContent = move.player;
                board[move.row][move.col] = move.player;
                moveCountElement.textContent = moveIndex + 1;
                moveIndex++;
            } else {
                clearInterval(replayInterval);
                clearInterval(gameInterval);
                gameDurationElement.textContent = gameEndTime;
            }
        }, 500);
    }

    /**
     * Získání obtížnosti
     * Nastavení velikosti hrací desky
     * Inicializace prázdné hrací desky
     * Aktualizace informací - Nastaví se zobrazení trvání hry na '0', Nastaví se zobrazení počtu tahů na '0', Tlačítko pro přehrání hry (replayGameBtn) se zakáže.....
     * Tímto způsobem funkce initializeBoardWithoutResettingMoves() připraví hrací plán pro novou hru, aniž by resetovala předchozí tahy.
     */

    function initializeBoardWithoutResettingMoves() {
        const difficulty = difficultySelect.value;
        if (difficulty === 'easy') {
            boardSize = 3;
        } else if (difficulty === 'medium') {
            boardSize = 5;
        } else {
            boardSize = 7;
        }

        board = Array(boardSize).fill().map(() => Array(boardSize).fill(null));
        boardElement.innerHTML = '';
        boardElement.style.gridTemplateColumns = `repeat(${boardSize}, 100px)`;
        for (let i = 0; i < boardSize * boardSize; i++) {
            const cell = document.createElement('div');
            cell.classList.add('cell');
            cell.dataset.index = i;
            boardElement.appendChild(cell);
        }
        boardSizeElement.textContent = `${boardSize}x${boardSize}`;
        currentPlayer = 'X';
        gameStartTime = new Date();
        clearInterval(gameInterval);
        gameInterval = setInterval(updateGameDuration, 1000);
        gameDurationElement.textContent = '0';
        moveCountElement.textContent = '0';
        replayGameBtn.disabled = true;
        gameOver = false;
    }

    newGameBtn.addEventListener('click', initializeBoard);
    replayGameBtn.addEventListener('click', replayGame);
    toggleModeBtn.addEventListener('click', () => {
        isTwoPlayerMode = !isTwoPlayerMode;
        toggleModeBtn.textContent = isTwoPlayerMode ? 'Přepnout na AI' : 'Přepnout na 2 hráče';
    });

    backgroundMusic.volume = 0.1;
    backgroundMusic.play();

    let isMuted = false;
    volumeControl.addEventListener('click', () => {
        if (isMuted) {
            backgroundMusic.play();
            volumeControl.textContent = '🔊';
        } else {
            backgroundMusic.pause();
            volumeControl.textContent = '🔈';
        }
        isMuted = !isMuted;
    });

    initializeBoard();

    const howToPlayBtn = document.querySelector('.how-to-play-btn');
    const instructionsDiv = document.querySelector('.instructions');

    howToPlayBtn.addEventListener('click', () => {
        if (instructionsDiv.style.display === 'none') {
            instructionsDiv.style.display = 'block';
        } else {
            instructionsDiv.style.display = 'none';
        }
    });
});